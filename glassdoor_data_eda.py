#%%
import pandas as pd
import matplotlib.pyplot as plt 
import seaborn as sns 

df = pd.read_csv('salary_data_cleaned.csv')
df.head()
df.columns

def title_siplified(title):
    if 'data scientist' in title.lower():
        return 'data scientist'
    elif 'data engineer' in title.lower():
        return 'data engineer'
    elif 'analyst' in title.lower():
        return 'analyst'
    elif 'machine learning' in title.lower():
        return 'mlea'
    elif 'manager' in title.lower():
        return 'manager'
    elif 'director' in title.lower():
        return 'director'
    else:
        return 'na'
def seniorty(title):
    if 'sr' in title.lower() or 'senior' in title.lower() or 'sr' in title.lower() or 'lead' in title.lower() or 'principal' in title.lower():
        return 'senior'
    elif 'jr' in title.lower() or 'jr.' in title.lower():
        return 'jenior'
    else:
        return 'na'
df['Job_simple'] = df['Job Title'].apply(title_siplified)
df['Job_simple'].value_counts()
df['seniorty'] = df['Job Title'].apply(seniorty)
df['seniorty'].value_counts()
df['Job_state'] = df['Job_state'].apply(lambda x: x.replace('Los Angeles', 'CA'))
df['Job_state'].value_counts()
df['Description_len'] = df['Job Description'].apply(lambda x: len(x))
df['num_comp'] = df['Competitors'].apply(lambda x: len(x.split(',')) if x != '-1' else 0)
df['num_comp'].value_counts()
df['min_salary'] = df.apply(lambda x: x.Min_salary*2 if x.Hourly == 1 else x.Min_salary, axis =1)
df['max_salary'] = df.apply(lambda x: x.Max_salary*2 if x.Hourly == 1 else x.Max_salary, axis = 1)
df[df.Hourly == 1][['Hourly', 'min_salary', 'max_salary']]
df['company_txt'] = df['company_txt'].apply(lambda x: x.replace('/n', ''))

df.describe()
df.Rating.hist()
df.Avg_salary.hist()
df.Age.hist()
df.Description_len.hist()
df.boxplot(column = ['Age','Avg_salary','Rating'])
df.boxplot(column = 'Rating')
df[['Age', 'Avg_salary', 'Rating', 'Description_len']].corr()
cmap = sns.diverging_palette(220, 10, as_cmap=True)
sns.heatmap(df[['Age','Avg_salary','Rating','Description_len','num_comp']].corr(),vmax=.3, center=0, cmap=cmap,
            square=True, linewidths=.5, cbar_kws={"shrink": .5})

df.columns
df_cat = df[['Location', 'Headquarters', 'Size', 'Sector', 'Ownership', 'Industry', 'Revenue', 'company_txt', 'Job_state', 'aws', 'excel', 'Same_satet', 'seniorty', 'Job_simple', 'Spark']]
for i in df_cat.columns:
    cat_num = df_cat[i].value_counts()[:20]
    print("chart for %s : tottal %d" % (i, len(cat_num)))
    chart = sns.barplot(x = cat_num.index , y = cat_num)
    chart.set_xticklabels(chart.get_xticklabels(), rotation=90)
    plt.show()

pd.pivot_table(df, index = 'Job_simple', values = 'Avg_salary')
pd.pivot_table(df, index = ['Job_simple', 'seniorty'], values = 'Avg_salary')
pd.pivot_table(df, index = ['Job_state', 'Job_simple', 'seniorty'], values= 'Avg_salary').sort_values('Job_state')
pd.pivot_table(df, index = ['Job_state', 'Job_simple', 'seniorty'], values= 'Avg_salary', aggfunc = 'count').sort_values('Job_state')
pd.pivot_table(df[df.Job_simple == 'data scientist'], index = 'Job_state', values = 'Avg_salary').sort_values('Avg_salary', ascending = False)
pd.pivot_table(df, index = ['Job_simple', 'Employer_provided', 'Ownership'] , values = 'Avg_salary').sort_values('Avg_salary', ascending = False)
pd.pivot_table(df, index = ['Job_simple', 'seniorty', 'Pythony', 'Rny'], values= 'Avg_salary')

# rating, industry, sector, revenue, number of comp, hourly, employer provided, python, r, spark, aws, excel, desc_len, Type of onwership
df_pivot = df[['Rating', 'Industry', 'Sector', 'Revenue', 'num_comp', 'Hourly', 'Employer_provided', 'Pythony', 'Rny', 'Spark', 'aws', 'excel', 'Description_len', 'Ownership', 'Avg_salary']]
for i in df_pivot.columns:
    if i == 'Avg_salary' : 
        break
    print(i)
    sort = pd.pivot_table(df_pivot, index = i, values = 'Avg_salary')
    print(sort)

pd.pivot_table(df_pivot, index = 'Revenue', columns = 'Pythony', values = 'Avg_salary')
# %%
