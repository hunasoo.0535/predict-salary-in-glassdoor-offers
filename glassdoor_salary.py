from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException, ElementClickInterceptedException
import time 
import pandas as pd




def get_jobs(keyword, num_jobs, slp_time):

    url = "https://www.glassdoor.com/Job/jobs.htm?suggestCount=0&suggestChosen=false&clickSource=searchBtn&typedKeyword="+keyword+"&sc.keyword="+keyword+"&locT=&locId=&jobType="
    options = webdriver.ChromeOptions()
    options.add_experimental_option("detach", True)
    driver = webdriver.Chrome(options=options)
    driver.get(url)
    jobs = []
    while len(jobs) < num_jobs:
        time.sleep(slp_time)

        # get all the job button  
        job_buttons = driver.find_elements(By.XPATH, "*//li[contains(@class, 'react-job-listing ')]")
        # loop through each job button on the current page 
        for job_button in job_buttons:
            # just print the progress that the scrup doses 
            print("Progress: {}".format("" + str(len(jobs)) + "/" + str(num_jobs)))
            if len(jobs) >= num_jobs:
                break
            # click each job_button in job_buttons
            driver.execute_script("arguments[0].click( )", job_button)
            time.sleep(5)

                    # test the sing up modal to get rid of it 
            try : 
                sign = driver.find_element(By.ID, "SignInButton")
                driver.execute_script("arguments[0].click( )", sign)
            except ElementClickInterceptedException:
                time.sleep(5)
            try : 
                close = driver.find_element(By.XPATH, "//span[contains(@alt,'Close')]")
                driver.execute_script("arguments[0].click( )", close)
            except NoSuchElementException: 
                time.sleep(5)

            # scrap company_name, job_title , location 
            collect_successfully = False
            while not collect_successfully :
                try :
                    company_name = driver.find_element(By.XPATH, '//div[@class="css-xuk5ye e1tk4kwz5"]').text
                    job_title = driver.find_element(By.XPATH, '//div[contains(@class, "css-1j389vi e1tk4kwz2")]').text
                    Location = driver.find_element(By.XPATH, '//div[contains(@class, "css-56kyx5 e1tk4kwz1")]').text
                    job_description = driver.find_element_by_xpath('//div[contains(@class, "jobDescriptionContent desc")]').text
                    collect_successfully = True
                except : 
                    time.sleep(5)
            
            try : 
                salary_estimate = driver.find_element(By.XPATH, '//span[@class="css-1hbqxax e1wijj240"]').text
            except NoSuchElementException :
                salary_estimate = -1
            
            try :
                rating : driver.find_element(By.XPATH, '//div[contains(@class, "mr-sm css-ey2fjr e1pr2f4f3")]').text
            except NoSuchElementException:
                rating = -1
            time.sleep(5)
            # go to company tab
           # try :
                #comp_tab = driver.find_element(By.XPATH, '//div[@data-test = "overview" and @data-item = "tab"]')
                #driver.execute_script("arguments[0].click( )", comp_tab)
            try : 
                size = driver.find_element(By.XPATH, '//div[@class = "d-flex justify-content-start css-daag8o e1pvx6aw2"]//span[text()="Size"]/following-sibling::span')
                size = size.text
            except NoSuchElementException:
                size = -1

            try : 
                founded = driver.find_element(By.XPATH, '//div[@class = "d-flex justify-content-start css-daag8o e1pvx6aw2"]//span[text()="Founded"]/following-sibling::span')
                founded = founded.text
            except NoSuchElementException:
                founded = -1

            try :
                type_of_ownership = driver.find_element(By.XPATH, '//div[@class = "d-flex justify-content-start css-daag8o e1pvx6aw2"]//span[text()="Type"]/following-sibling::span')
                type_of_ownership = type_of_ownership.text
            except NoSuchElementException:
                type_of_ownership = -1

            try :
                industry = driver.find_element(By.XPATH, '//div[@class = "d-flex justify-content-start css-daag8o e1pvx6aw2"]//span[text()="TypIndustrye"]/following-sibling::span')
                industry = industry.text
            except NoSuchElementException:
                industry = -1

            try :
                revenue = driver.find_element(By.XPATH, '//div[@class = "d-flex justify-content-start css-daag8o e1pvx6aw2"]//span[text()="Revenue"]/following-sibling::span')
                revenue = revenue.text
            except NoSuchElementException:
                revenue = -1

            try : 
                sector = driver.find_element(By.XPATH, '//div[@class = "d-flex justify-content-start css-daag8o e1pvx6aw2"]//span[text()="Revenue"]/following-sibling::span')
                sector = sector.text
            except NoSuchElementException:
                sector = -1

            try:
                competitors = driver.find_element_by_xpath('.//div[@class="infoEntity"]//label[text()="Competitors"]//following-sibling::*').text
            except NoSuchElementException:
                competitors = -1

            try :
                headquarters = driver.find_element_by_xpath('.//div[@class="infoEntity"]//label[text()="Headquarters"]//following-sibling::*').text
            except NoSuchElementException:
                headquarters = -1

            #except NoSuchElementException:
                #print("Wrong")

            jobs.append({"Company Name" : company_name,
                "Job Title" : job_title,
                "Location" : Location,
                "Salary" : salary_estimate,
                "Size" : size,
                "Founded" : founded,
                "Ownership" : type_of_ownership,
                "Industry" : industry,
                "Revenue" : revenue,
                "Sector" :  sector
            })
            # go to the next page 
        try : 
            next_page = driver.find_element(By.XPATH, '//button[@class = "page selected css-1hq9k8 e13qs2071"]/following-sibling::button')
            driver.execute_script("arguments[0].click( )",next_page)
        except NoSuchElementException:
            print("Scraping terminated before reaching target number of jobs. Needed {}, got {}.".format(num_jobs, len(jobs)))
            break

    return pd.DataFrame(jobs)



