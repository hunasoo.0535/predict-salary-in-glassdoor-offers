#%%
import pandas as pd 
import matplotlib.pyplot as plt 
import numpy as np 

df = pd.read_csv('eda_data.csv')

df.columns

df_model = df[['Rating', 'Headquarters', 'Size', 'Industry', 'Ownership', 'Sector', 'Revenue', 'Job_simple', 'num_comp', 'seniorty', 'Hourly', 'Employer_provided', 'Avg_salary', 'Age', 'company_txt', 'Job_state', 'Same_satet', 'Pythony', 'Rny', 'Spark', 'aws', 'excel']]
df_dum = pd.get_dummies(df_model)
from sklearn.model_selection import train_test_split
X = df_dum.drop('Avg_salary', axis =1)
Y = df_dum.Avg_salary.values
X_train, X_test, Y_train, Y_test = train_test_split(X, Y)
import statsmodels.api as sm
X_sm = X = sm.add_constant(X)
model = sm.OLS(Y, X_sm)
model.fit().summary()
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import Lasso
from sklearn.model_selection import cross_val_score

lm = LinearRegression()
lm.fit(X_train, Y_train)
np.mean(cross_val_score(lm, X_train, Y_train, scoring = 'neg_mean_absolute_error', cv = 3))

lm_l = Lasso(alpha = .13)
lm_l.fit(X_train, Y_train)
np.mean(cross_val_score(lm_l, X_train, Y_train, scoring = 'neg_mean_absolute_error', cv = 3))
error= []
alpha= []

for i in range(1, 100):
    alpha.append(i/100)
    lml = Lasso(alpha = (i/100))
    error.append(np.mean(cross_val_score(lml, X_train, Y_train, scoring = 'neg_mean_absolute_error', cv = 3)))
plt.plot(alpha, error)
err = tuple(zip(alpha, error))
df_err = pd.DataFrame(err, columns = ['alpha', 'error'])
df_err[df_err.error == max(df_err.error)]


from sklearn.ensemble import RandomForestRegressor
rf = RandomForestRegressor()

np.mean(cross_val_score(rf,X_train,Y_train,scoring = 'neg_mean_absolute_error', cv= 3))

# tune models GridsearchCV 
from sklearn.model_selection import GridSearchCV
parameters = {'n_estimators':range(10,300,10), 'criterion':('mse','mae'), 'max_features':('auto','sqrt','log2')}

gs = GridSearchCV(rf,parameters,scoring='neg_mean_absolute_error',cv=3)
gs.fit(X_train,Y_train)

gs.best_score_
gs.best_estimator_

# test ensembles 
tpred_lm = lm.predict(X_test)
tpred_lml = lm_l.predict(X_test)
tpred_rf = gs.best_estimator_.predict(X_test)

from sklearn.metrics import mean_absolute_error
mean_absolute_error(Y_test,tpred_lm)
mean_absolute_error(Y_test,tpred_lml)
mean_absolute_error(Y_test,tpred_rf)

mean_absolute_error(Y_test,(tpred_lm+tpred_rf)/2)

import pickle
pickl = {'model': gs.best_estimator_}
pickle.dump( pickl, open( 'file_predict_model' + ".p", "wb" ) )

file_name = "file_predict_model.p"
with open(file_name, 'rb') as pickled:
    data = pickle.load(pickled)
    model = data['model']

model.predict(np.array(list(X_test.iloc[1,:])).reshape(1,-1))[0]

list(X_test.iloc[1,:])

# %%
