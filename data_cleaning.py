#%%
import pandas as pd

df = pd.read_csv('glassdoor_jobs.csv')
df.rename(columns = {'Salary Estimate':'Salary', 'Type of ownership':'Ownership'}, inplace = True)
del df['Unnamed: 0']
df['Salary'] = df['Salary'].apply(lambda x: x.replace('(Glassdoor est.)', '').replace('(Employer est.)', ''))
df['Hourly'] = df['Salary'].apply(lambda x: 1 if 'per hour' in x.lower() else 0)
df['Employer_provided'] = df['Salary'].apply(lambda x: 1 if 'Employer Provided Salary:' in x else 0)
df = df[df['Salary'] != '-1']
df['Salary'] = df['Salary'].apply(lambda x: x.replace('K', '').replace('$', '').replace('Employer Provided Salary:', '').replace('Per Hour', ''))
df['Min_salary'] = df['Salary'].apply(lambda x: int(x.split('-')[0]))
df['Max_salary'] = df['Salary'].apply(lambda x: int(x.split('-')[1]))
df['Avg_salary'] = (df['Max_salary'] + df['Min_salary'])/2
df['Age'] = df['Founded'].apply(lambda x: x if x < 1 else 2022 - x)
df['company_txt'] = df.apply(lambda x: x['Company Name'] if x['Rating'] <0 else x['Company Name'][:-3], axis = 1)
df['Job_state'] = df['Location'].str.split(',').str[1]
df['Job_state'].value_counts()
df['Same_satet'] = df.apply(lambda x: 1 if x.Location == x.Headquarters else 0, axis = 1)
df['Pythony'] = df['Job Description'].apply(lambda x: 1 if 'python' in x.lower() else 0)
df['Rny'] = df['Job Description'].apply(lambda x: 1 if 'r studio' in x.lower() or 'r-studio' in x.lower() else 0)
df['Rny'].value_counts()
df['Spark'] = df['Job Description'].apply(lambda x: 1 if 'spark' in x.lower() else 0)
df['Spark'].value_counts()
df['aws'] = df['Job Description'].apply(lambda x: 1 if 'aws' in x.lower() else 0)
df['aws'].value_counts()
df['excel'] = df['Job Description'].apply(lambda x: 1 if 'excel' in x.lower() else 0)
df['excel'].value_counts()
df.columns
df.to_csv('salary_data_cleaned.csv',index = False)
# %%
